<?php

namespace QingSen\message;

use GuzzleHttp\Exception\GuzzleException;

/**
 * 群、聊天室、群组
 */
class Room
{
    use MessagePost;

    public const URI_NEW = 'group/new';
    public const URI_ADD_MEMBER = 'group/add_member';

    public function __construct(string $key, string $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * 创建新群
     * @param string $adminUID 管理员索引号或创建者的索引
     * @param string $name 群组名
     * @param string $introduction 描述
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function add(string $adminUID, string $name, string $introduction = '')
    {
        $data               = [
            'admin_uid'     => $adminUID,
            'name'          => $name,
            'introduction'  => $introduction,
        ];
        return $this->post(self::URI_NEW, $data);
    }
}
