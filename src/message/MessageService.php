<?php

namespace QingSen\message;

class MessageService
{
    public static function user(string $key, string $secret)
    {
        return new User($key, $secret);
    }
    public static function room(string $key, string $secret)
    {
        return new Room($key, $secret);
    }
    public static function notify(string $key, string $secret)
    {
        return new Notify($key, $secret);
    }
    public static function system(string $key, string $secret)
    {
        return new System($key, $secret);
    }
}
