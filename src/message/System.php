<?php

namespace QingSen\message;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 发送系统类消息
 */
class System
{
    use MessagePost;

    public const URI_SCOPE  = 'system/to_uid';

    public function __construct(string $key, string $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * 发送消息给多个账号
     * @param array $uid 账号索引
     * @param array $message 消息体
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function scopes(array $uid, array $message)
    {
        $data           = [
            'uid'       => $uid,
            'message'   => $message,
        ];
        return $this->post(self::URI_SCOPE, $data);
    }
}
