<?php

namespace QingSen\message;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

class User
{
    use MessagePost;

    public const URI_ADD    = 'user/add';
    public const URI_DEL    = 'user/del';
    public const URI_ONLINE = 'user/query_online';

    public function __construct(string $key, string $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }
    /**
     * 添加账号
     *
     * @param string $uid 账号索引号
     * @param string $nick 用户昵称
     * @param string $face 头像地址
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function add(string $uid, string $nick, string $face, int $role = 100)
    {
        $data       = [
            'uid'   => $uid,
            'nick'  => $nick,
            'face'  => $face,
            'role'  => $role,
        ];
        return $this->post(self::URI_ADD, $data);
    }
    /**
     * 删除账号
     * @param string $uid
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function del(string $uid)
    {
        $data       = [
            'uid'   => $uid,
        ];
        return $this->post(self::URI_DEL, $data);
    }
    /**
     * 批量查询用户是否在线
     * @param array $uid
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function online(array $uid = [])
    {
        $data       = [
            'uid'   => $uid,
        ];
        return $this->post(self::URI_ONLINE, $data);
    }
}
