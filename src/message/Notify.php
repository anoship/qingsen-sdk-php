<?php

namespace QingSen\message;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 发送通知类消息
 */
class Notify
{
    use MessagePost;

    public const URI_SCOPE  = 'notify/to_uid';
    public const URI_ALL    = 'notify/to_all';

    public function __construct(string $key, string $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * 发送消息给多个账号
     * @param array $uid 账号索引
     * @param array $message 消息体
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function scope(array $uid, array $message)
    {
        $data           = [
            'uid'       => $uid,
            'message'   => $message,
        ];
        return $this->post(self::URI_SCOPE, $data);
    }
    /**
     * 发送消息给全部在线的客户端
     * @param array $message
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function all($message)
    {
        return $this->post(self::URI_SCOPE, $message);
    }
}
