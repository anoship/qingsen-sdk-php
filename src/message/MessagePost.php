<?php

namespace QingSen\message;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

const BASE_URI = 'https://im.kanghao.shop';

trait MessagePost
{
    protected $key;
    protected $secret;

    /**
     * post
     * @param string $uri
     * @param array $params
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function post(string $uri, array $params)
    {
        $client         = new Client([
            'base_uri'  => BASE_URI,
            'auth'      => [$this->key, $this->secret]
        ]);

        $option         = [
            \GuzzleHttp\RequestOptions::FORM_PARAMS => $params,
        ];

        return $client->post($uri, $option);
    }
}
