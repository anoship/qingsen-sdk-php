<?php

namespace QingSen\gridfs;

class VideoMetadata extends AbstractMetadata
{
    public $type; // 所属文件类别 固定值video
    public $ext; // 视频的扩展名
    public $mime; // 视频类型
    public $user; // 所属用户索引
    public $width; // 视频尺寸宽
    public $height; // 视频尺寸高
    public $second; // 视频时长
    public $preview_id; // 截图索引号
    public $id;
    public $filename;
    public $link;
    /** @var ImageMetadata */
    public $preview;

    public function jsonSerialize()
    {
        return (array) $this;
    }
}
