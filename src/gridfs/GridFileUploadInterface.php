<?php

namespace QingSen\gridfs;

/**
 * 上传处理器
 */
interface GridFileUploadInterface
{
    /**
     * 上传图片
     *
     * @see http://file.master.cn/document/image_upload
     * @return ImageMetadata
     */
    public function image();

    /**
     * 格式化图片
     *
     * @see http://file.master.cn/document/image_format
     * @param int $quality 压缩比
     * @param string $format 期望得到的格式后缀
     * @return ImageMetadata
     */
    public function imageFormat($quality = 100, $format = 'jpg');

    /**
     * 裁剪图片
     *
     * @see http://file.master.cn/document/image_crop
     * @param int $width 宽度
     * @param int $height 高度
     * @param int|null $x 水平起始位置
     * @param int|null $y 垂直起始位置
     * @param int $quality 压缩比
     * @param string $format 期望得到的格式后缀
     * @return ImageMetadata
     */
    public function imageCrop($width, $height, $x = null, $y = null, $quality = 100, $format = 'jpg');

    /**
     * 缩放并裁剪图片
     *
     * --template 尺寸模板
     *
     *      IMAGE_TEMPLATE_SMALL           = 'small';
     *
     *      IMAGE_TEMPLATE_MEDIUM          = 'medium';
     *
     *      IMAGE_TEMPLATE_LARGE           = 'large';
     *
     *      IMAGE_TEMPLATE_LARGE16BY9      = 'large16by9';
     *
     *      IMAGE_TEMPLATE_AVATAR          = 'avatar';
     *
     *      IMAGE_TEMPLATE_ICON32          = 'icon32';
     *
     *      IMAGE_TEMPLATE_ICON64          = 'icon64';
     *
     *      IMAGE_TEMPLATE_ICON128         = 'icon128';
     *
     *      IMAGE_TEMPLATE_ICON256         = 'icon256';
     *
     *      IMAGE_TEMPLATE_WH100           = 'wh100';
     *
     *      IMAGE_TEMPLATE_WH200           = 'wh200';
     *
     *      IMAGE_TEMPLATE_WH300           = 'wh300';
     *
     *      IMAGE_TEMPLATE_WH400           = 'wh400';
     *
     * @see http://file.master.cn/document/image_fit
     * @param string $template 尺寸模板
     * @param int $position 位置
     * @param int $quality 压缩率
     * @param string $format 期望得到的图片后缀
     * @return ImageMetadata
     */
    public function imageFit($template = 'small', $position = 5, $quality = 100, $format = 'jpg');

    /**
     * 缩放图片
     *
     * --template 尺寸模板
     *
     *  IMAGE_TEMPLATE_W100            = 'w100';
     *
     *  IMAGE_TEMPLATE_W200            = 'w200';
     *
     *  IMAGE_TEMPLATE_W300            = 'w300';
     *
     *  IMAGE_TEMPLATE_W400            = 'w400';
     *
     *  IMAGE_TEMPLATE_H100            = 'h100';
     *
     *  IMAGE_TEMPLATE_H200            = 'h200';
     *
     *  IMAGE_TEMPLATE_H300            = 'h300';
     *
     *  IMAGE_TEMPLATE_H400            = 'h400';
     *
     * @see http://file.master.cn/document/image_resize
     * @param string $template 尺寸模板
     * @param int $template 尺寸模板
     * @param int $quality 压缩比例
     * @param string $format 期望得到的图片后缀
     * @return ImageMetadata
     */
    public function imageResize($template = 'w100', $quality = 100, $format = 'jpg');

    /**
     * 分层拼贴图片
     *
     * @see http://file.master.cn/document/image_insert
     * @param int $quality 压缩比
     * @param string $format 期望得到的图片后缀
     * @param int $position layer_1位置
     * @param int $x layer_1水平偏移量
     * @param int $y layer_1垂直偏移量
     * @return ImageMetadata
     */
    public function imageInsert($quality = 100, $format = 'jpg', $position = 1, $x = 0, $y = 0);

    /**
     * 上传音频
     *
     * @see http://file.master.cn/document/audio_upload
     * @return AudioMetadata
     */
    public function audio();
    /**
     * 转印音频文件为mp3
     *
     * @see http://file.master.cn/document/audio_transcode
     * @param string $format 期望得到的音频格式
     * @param int[] $clip 截取时间段起始时间到结束时间
     * @param int $channels 通道
     * @param int $kbps 编码速率
     * @return AudioMetadata
     */
    public function audioTranscode($format = 'mp3', $clip = [], $channels = 2, $kbps = 256);

    /**
     * 上传普通文件
     *
     * @see http://file.master.cn/document/file_upload
     * @return FileMetadata
     */
    public function file();

    /**
     * 上传视频
     *
     * @see http://file.master.cn/document/video_upload
     * @return VideoMetadata
     */
    public function video();
}
