<?php

namespace QingSen\gridfs;

class AudioMetadata extends AbstractMetadata
{
    public $type;
    public $mime;
    public $ext;
    /** @var int */
    public $user;
    public $second;
    public $id;
    public $filename;
    public $link;

    public function jsonSerialize()
    {
        return (array) $this;
    }
}
