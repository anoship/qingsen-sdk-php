<?php

namespace QingSen\gridfs;

/**
 * 文件元信息 图片信息
 */
class ImageMetadata extends AbstractMetadata
{
    public $type;
    public $ext;
    public $mime;
    /** @var int */
    public $user;
    /** @var int */
    public $width;
    /** @var int */
    public $height;
    public $id;
    public $filename;
    public $link;

    public function jsonSerialize()
    {
        return (array) $this;
    }
}
