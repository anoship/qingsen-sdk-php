<?php

namespace QingSen\gridfs;

/**
 * 文件管理处理器
 */
interface GridFileManagerInterface
{
    public function doc($filter, $option);
    public function query($filter, $option);
    public function delete();
}
