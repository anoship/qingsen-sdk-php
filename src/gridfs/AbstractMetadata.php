<?php

namespace QingSen\gridfs;

use JsonSerializable;

abstract class AbstractMetadata implements JsonSerializable
{
    /**
     * 将字符串转换为相对应元信息数据类型
     *
     * @param string $content
     * @return static
     * @throws \Exception
     */
    public static function from(string $content)
    {
        $static         = new static();
        $data           = json_decode($content, true);

        if (false == is_array($data)) {
            throw new \Exception('解析失败！');
        }

        foreach ($data as $prop => $value) {
            $static->{$prop} = $value;
        }

        return $static;
    }
}
