<?php

namespace QingSen\gridfs;

class FileMetadata extends AbstractMetadata
{
    public $type;
    /** @var int */
    public $user;
    public $ext;
    public $mime;
    public $id;
    public $filename;
    public $link;

    public function jsonSerialize()
    {
        return (array) $this;
    }
}
